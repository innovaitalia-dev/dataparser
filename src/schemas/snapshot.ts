import mongoose, { Schema, Document, SchemaDefinition } from "mongoose";
import { ISnapshot, Snapshot } from "../models/snapshot";

const snapshotFields: ISnapshot = {
  timestamp: { type: Date, default: Date.now() },
  data: Schema.Types.Mixed,
  userId: { type: mongoose.Schema.Types.ObjectId, ref: "UserDoc" }
};
const SnapshotSchema: Schema = new Schema(
  (snapshotFields as unknown) as SchemaDefinition
);
export type SnapshotDocType = Snapshot & Document;
// Export the model and return your IUser interface
export const SnapshotDoc = mongoose.model<SnapshotDocType>(
  "SnapshotDoc",
  SnapshotSchema,
  "snapshots"
);
