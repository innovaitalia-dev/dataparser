import mongoose, { Schema, Document, SchemaDefinition } from "mongoose";
import { ITemplate, Template } from "../models/template";

const TemplateFields: ITemplate = {
  name: { type: String, default: "" },
  fields: Schema.Types.Mixed
};
const TemplateSchema: Schema = new Schema(
  (TemplateFields as unknown) as SchemaDefinition
);
export type TemplateDocType = Template & Document;
// Export the model and return your IUser interface
export const TemplateDoc = mongoose.model<TemplateDocType>(
  "ParsingTemplateDoc",
  TemplateSchema,
  "templates"
);
