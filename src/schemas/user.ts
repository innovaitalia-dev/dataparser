import mongoose, { Schema, Document, SchemaDefinition } from "mongoose";
import { IUser, User } from "../models/user";
import moment from "moment";

const userFields: IUser = {
  email: { type: String, required: true, unique: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  birthDate: { type: Date, required: true },
  phoneNumber: { type: String, required: true },
  emergencyPhoneNumber: { type: String, required: false },

  password: { type: String, required: true },
  sex: { type: Number, required: true },
  personalNumber: { type: String, required: true },
  domicileCity: { type: String, required: true },
  domicileAddress: { type: String, required: true },
  residencyCity: { type: String, required: true },
  residencyAddress: { type: Object, required: true },
  openID: { type: String, required: false },
  lastUpdate: { type: Date },
  lastWeeklyUpdate: { type: Date }
};

const UserSchema: Schema = new Schema(
  (userFields as unknown) as SchemaDefinition
);
UserSchema.virtual('age').get(function() {
  return moment().diff(moment(this.birthDate),"years");
});
export type UserDocType = User & Document;
// Export the model and return your IUser interface
export const UserDoc = mongoose.model<UserDocType>("UserDoc", UserSchema, "users");
