import { injectable } from "inversify";
import { Template, ITemplateFieldData, TemplateFieldData, IParsedDataModel, ParsedDataModel } from "../models/template";
import { TemplateDoc } from "../schemas/template";
import { getSchemaByCollectionName, getAvailableCollection } from "../utils/environment";
import { getSingleKey, getJSONFormatStringKey } from "../utils/strings";
import { createErrorResponse } from "../utils/routes";
import container from "../di/inversify.config";

@injectable()
export class ParsingService {
  constructor() {}
  
  async getTemplateByName(templateName : string) {

    let template : Template | any;
    await TemplateDoc.findOne({name: templateName}).exec()
    .then((value : Template) => {
      if(value != null || value != undefined) {
        template = value;
      } else {
        template = null;
      }
    }).catch(error => {
        template = error;
    });
    return template;
  }

  getCollectionKeysFromRequest(request : any) : [{[key: string] : {[key : string] : string}}] {
    let response : [{[key: string] : {[key : string] : string}}];
    for(const key of request.keys) {
      response[key.collection] = {
        [key.keyName] : key.keyValue
      };
    }
    return response;
  }

  async getParsedData(template: Template, extractionKeys: any[]) {

    if(extractionKeys.length == 0) {
      return {errorStatus: 500, errorKey: "EXTRACTIONKEYS_NEEDED", errorMessage: "Key not found for needed collection extraction" };
    }
    let fields : TemplateFieldData[] = (await this.getDataFromCollections(template, extractionKeys));
    return this.structureDataFields(fields);
  }

  private async getDataFromCollections(template: Template, extractionKeys: any[]) {

    let fields : TemplateFieldData[] = [];
    for(const collection of getAvailableCollection()) {
      
      let tempFields : TemplateFieldData[] =  template.fields.filter(field => {
        return field.fromCollection == collection;
      });

      if(tempFields.length == 0) {
        continue;
      }

      let extractionKey = extractionKeys.filter((key: any) => {
        return (key.collection != undefined && key.collection == collection);
      });

      if(extractionKey.length == 0) {
        return null;
      }

      await getSchemaByCollectionName(collection).findOne({[extractionKey[0].keyName] : extractionKey[0].keyValue}).exec()
      .then((data: any) => {
        tempFields.forEach(function(field) {

          let responseValue = data;
          getSingleKey(field.fromKey).forEach((singleKey : string) => {
            responseValue = responseValue[singleKey];
          });
          field.valueCatched = responseValue;
          fields.push(field);
        });

      });
    }
    return fields;
  }

  private reloadLevelStructureDatas(fields: TemplateFieldData[]) {
    fields.forEach(field => {
      fields.forEach(field2 => {
        if(field2.toKey != field.toKey && field2.container != undefined && field.container != '' && field2.container.indexOf(field.toKey) > -1)
          field2.container = field.container + '.' + field2.container;
      });
    });
  }

  private structureDataFields(fields: TemplateFieldData[]) : ParsedDataModel[]{
    let response: ParsedDataModel[] = [];

    this.reloadLevelStructureDatas(fields);

    fields.forEach(field => {
      if(field.container == '') {
        let data : ParsedDataModel = {
          displayName: field.toKey,
          labelName: getJSONFormatStringKey(field.toKey)
        };
        if(field.valueCatched != undefined) {
          data.value = field.valueCatched;
        }
        response.push(data);
      } else {
        response.forEach((obj : ParsedDataModel) => {
          if(obj.displayName == getSingleKey(field.container)[0]) {
            let containerTemp: ParsedDataModel = obj;
            getSingleKey(field.container).forEach((label, index) => {
              if(index > 0) {
                if(containerTemp.subFields == undefined) {
                  containerTemp.subFields = [];
                  containerTemp.subFields.push({ 
                      displayName: label,
                      labelName: getJSONFormatStringKey(label)
                    });
                    containerTemp = containerTemp.subFields[0];
                } else {
                  containerTemp = containerTemp.subFields.filter((element: ParsedDataModel) => {
                    return (element.displayName == label);
                  })[0];
                }
              }
            });
            if(containerTemp.subFields == undefined) {
              containerTemp.subFields = [];
            }

            let data : IParsedDataModel = {
              displayName: field.toKey,
              labelName: getJSONFormatStringKey(field.toKey)
            }
            if(field.valueCatched != undefined) {
              data.value = field.valueCatched;
            }
            containerTemp.subFields.push(data);
          }
        });
      }     
    });
    return response;
  }
}