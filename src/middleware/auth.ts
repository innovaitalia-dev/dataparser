import passport from "passport";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import config from "../config";
import { User } from "../models/user";
import { UserDoc } from "../schemas/user";

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwt.secret
};

passport.use(
  "jwtStrategy",
  new JwtStrategy(opts, function(jwt_payload, done) {
    UserDoc.findOne({ email: jwt_payload.email }, function(err, user: User) {
      if (err) {
        return done(err, false);
      }
      if (user) {
        return done(null, user);
      } else {
        return done(null, false);
      }
    });
  })
);
export const CheckUserMiddleware = passport.authenticate("jwtStrategy", {
  session: false
});
