import { Router } from "express";
import { createResponse, createErrorResponse } from "../utils/routes";
import container from "../di/inversify.config";
import { ParsingService } from "../services/parsing.service";
import { SERVICE_TYPES } from "../di/types";
import { Template } from "../models/template";
import { celebrate, Segments, Joi } from "celebrate";

const router = Router();

const parsingRequestSchema = {
    templateName: Joi.string().required(),
    keys: Joi.array().items(Joi.object().keys({
        collection: Joi.string().required(),
        keyName: Joi.string().required(),
        keyValue: Joi.string().required()
    })).required()
  };

router.post("/",
celebrate({
    [Segments.BODY]: Joi.object().keys(parsingRequestSchema)
  }),
async (req, res, next) => {
        let parsingService = container.get<ParsingService>(
            SERVICE_TYPES.Parsing
          );
        if(req.body.templateName != undefined) {
            await parsingService.getTemplateByName(req.body.templateName)
            .then(async (template : Template) => {
                if(req.body.keys != undefined && req.body.keys.length > 0) {
                    await parsingService.getParsedData(template, req.body.keys)
                    .then(body => {
                            createResponse(res, "Response Object created", body);
                    }).catch(error => {
                        next(error);
                    });
                } else {
                        createErrorResponse(res, "EXTRACTIONKEYS_NOT_FOUND", "Extraction keys has not been found in parameters");
                }
                
            }).catch(error => {
                next(error);
            });
        } else {
            createErrorResponse(res, "NO_TEMPLATENAME_FOUND", "No templateName has been found in the request parameters");
        }
});

router.get("/prova",
async (req, res, next) => {
    res.json({message:"hello my friend"});
});

export default router;