import { Router } from "express";
import mobileRouter from "../routes/mobile/index";
import webRouter from "../routes/web/index";
import parseRouter from "./parse";

const rootUrl = "/dataparser/api";

export default () => {
  const appRouter = Router();
  appRouter.use(rootUrl+"/mobile", mobileRouter);
  appRouter.use(rootUrl+"/web", webRouter);
  appRouter.use(rootUrl+"/parsing", parseRouter);
  return appRouter;
};
