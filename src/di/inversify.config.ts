import "reflect-metadata";
import {Container} from "inversify";
import {SERVICE_TYPES} from "./types";
import { Db } from "../services/db.service";
import { ParsingService } from "../services/parsing.service";
let container = new Container();

container.bind<Db>(SERVICE_TYPES.Db).to(Db).inSingletonScope();
container.bind<ParsingService>(SERVICE_TYPES.Parsing).to(ParsingService).inSingletonScope();

export default container;