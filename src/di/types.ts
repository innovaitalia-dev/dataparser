export const SERVICE_TYPES = {
    Db:Symbol("DatabaseService"),
    Conversation:Symbol("ConversationService"),
    User:Symbol("UserService"),
    Parsing:Symbol("ParsingService")

  };