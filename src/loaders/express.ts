import express from "express";
import bodyParser from 'body-parser';
import morgan from "morgan";
import routes from "../routes";
export default ({ app }: { app: express.Application }) => {
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "*");
        if (req.method === "OPTIONS") {
            res.header("Access-Control-Allow-Methods", "*");
            return res.status(200).json({});
        }
        next();
    });

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(morgan("dev"));

    app.use(routes());

    app.use((req, res, next) => {
        let error = new Error("Not found");
        res.statusCode = 404;
        next(error);
    });
    app.use((error, req, res, next) => {
        let resStatus=res.statusCode || error.status || 500;
        if (res.statusCode == 200)
            res.status(500);
        res.status(res.statusCode || error.status || 500);
        console.log({
            error: error.errors || {
                message: error.message
            }
        })
        res.json({
            code:resStatus,
            message: error.errors || error.message,

        });
    });
}
