import dotenv from "dotenv";
import dotenvExpand from "dotenv-expand";

const envFound = dotenv.config();
dotenvExpand(envFound);
if (!envFound) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
  env: process.env.NODE_ENV === "dev" ? "DEV" : "PROD",
  port: parseInt(process.env.PORT ? process.env.PORT : "3000", 10),

  logs: {
    level: process.env.LOG_LEVEL
  },
  api: {
    prefix: ""
  },
  db: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    name: process.env.DB_NAME,
    port: process.env.DB_PORT,
    uri: process.env.DB_URI
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    exp: process.env.JWT_EXP
  },
  assistant: {
    url: process.env.ASSISTANT_URL,
    apiKey: process.env.ASSISTANT_API_KEY
  }
};
