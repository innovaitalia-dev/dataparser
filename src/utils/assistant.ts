"use strict";

import moment from "moment";
import AssistantV2 from "ibm-watson/assistant/v2";
import { IamAuthenticator, BasicAuthenticator } from "ibm-watson/auth";

interface IPayload {
  dialogId: string;
  businessJson: any;
  userId: string;
}
export class Assistant {
  originalResponse: any;
  assistant: AssistantV2;
  sessions: {};
  sessionTimeout: any;
  sessionClearInterval: number;
  /**
   * Constructor
   * @param {Object} options - contains credentials, originalResponse, sessionTimeout, logger
   * @param {Object} options.credentials - credentials object: username, password and apikey are its properties.
   * @param {boolean} options.originalResponse - ability to returns the original response inside field original_response.
   * @param {number} options.sessionTimeout - session duration in minutes. If not provided, it is set to 5 minutes.
   * @constructor
   */
  constructor(options: {
    credentials: { username?; password?; url } | { apikey; url };
    originalResponse?: any;
    sessionTimeout?: number;
  }) {
    this.originalResponse = options.originalResponse || false;

    let authenticator;
    if (options.credentials && options.credentials["apikey"]) {
      authenticator = new IamAuthenticator({
        apikey: options.credentials["apikey"]
      });
    } else if (
      options.credentials &&
      options.credentials["username"] &&
      options.credentials["password"]
    ) {
      authenticator = new BasicAuthenticator({
        username: options.credentials["username"],
        password: options.credentials["password"]
      });
    } else {
      throw new Error(`Can't instantiate Discovery without valid credentials`);
    }

    this.assistant = new AssistantV2({
      authenticator: authenticator,
      version: `2019-02-28`,
      url: options.credentials.url,
      headers: { "X-Watson-Learning-Opt-Out": `true` }
    });

    this.sessions = {};
    this.sessionTimeout = options.sessionTimeout || 5; // defaults to 5 minutes
    this.sessionClearInterval = 5 * 1000; // 5 seconds
    setInterval(() => {
      const now = moment();
      const keys = Object.keys(this.sessions);
      for (const k of keys) {
        if (this.sessions[k].timeout.isBefore(now)) {
          delete this.sessions[k];
        }
      }
    }, this.sessionClearInterval);
  }

  /**
   * @param {Object} payload - contains dialogId, businessJson, userId
   * @param {string} payload.dialogId - assistant id
   * @param {Object} payload.businessJson - dialog context coming from U-GOV systems
   * @param {string} payload.userId - user identifier
   * @param {string} text - message to be sent
   * @returns {Promise}
   */
  async message(payload: IPayload, text: string) {
    return this._getSession(payload.dialogId, payload.userId).then(
      sessionId => {
        return this._sendMessage(
          payload.dialogId,
          sessionId,
          text,
          payload.businessJson
        ).catch(error => {
          if (error.code === 404) {
            return this._getSession(
              payload.dialogId,
              payload.userId,
              true
            ).then(sessionId => {
              return this._sendMessage(
                payload.dialogId,
                sessionId,
                text,
                payload.businessJson
              );
            });
          } else {
            return Promise.reject(error);
          }
        });
      }
    );
  }

  /**
   * @param {string} dialogId - assistant id
   * @param {string} userId - user identifier
   * @param {boolean=false} force - force generating a new session
   * @returns {Promise}
   */
  private async _getSession(dialogId, userId, force?) {
    const now = moment();
    const s = this.sessions[userId];
    if (!force && s && s.timeout.isAfter(now)) {
      this.sessions[userId].timeout= this.sessions[userId].timeout.add(this.sessionTimeout, `m`);
      return Promise.resolve(this.sessions[userId].sessionId);
    } else {
      return this.assistant
        .createSession({ assistantId: dialogId })
        .then(response => {
          this.sessions[userId] = {
            sessionId: response.result.session_id,
            timeout: now.add(this.sessionTimeout, `m`)
          };
          return this.sessions[userId].sessionId;
        });
    }
  }

  /**
   * @param {string} assistantId - assistant id
   * @param {string} sessionId - session id for a specific assistant
   * @param {string} text - message
   * @param {Object} context - dialog context
   * @returns {Promise}
   */
  private async _sendMessage(assistantId, sessionId, text, context) {
    return this.assistant.message({
      assistantId: assistantId,
      sessionId: sessionId,
      input: {
        message_type: `text`,
        text: text,
        options: {
          debug: true,
          // restart: false,
          alternate_intents: true,
          return_context: true
        }
      },
      context: {
        skills: {
          "main skill": {
            user_defined: { data: context }
          }
        }
      }
    });
  }

  private static _extractText(generic) {
    const text = [];
    for (const g of generic) {
      if (g.response_type === `text`) {
        text.push(g.text);
      }
    }
    return text;
  }
}
