import { workerData } from "worker_threads";

export function getSingleKey(stringa : string) : string[] {
    let response : string[];
    response = stringa.split(".");
    return response;
}

export function getJSONFormatStringKey(stringa: string) : string {
  let response : string = "";
  let words = stringa.split(" ");
  response = words[0].normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  for(let i=1; i<words.length; i++) {
    response = response + toCapitalize(words[i]).normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }
  return response;
}

export function toCapitalize(stringa: string) {
  return stringa.charAt(0).toUpperCase() + stringa.slice(1);
}