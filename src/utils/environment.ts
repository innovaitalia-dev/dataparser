import { ModelTypes } from "../models/modeltypes";
import { TemplateDoc } from "../schemas/template";
import { UserDoc } from "../schemas/user";
import { SnapshotDoc } from "../schemas/snapshot";

export const DBSchemalList : ModelTypes[] = [
    {    
      name:"TemplateDoc",
      schema: TemplateDoc,
      collection: "templates"
    },
    {
      name:"UserDoc",
      schema: UserDoc,
      collection: "users"
    },
    {
      name:"SnapshotDoc",
      schema: SnapshotDoc,
      collection: "snapshots"
    }
  ]
  
  export function getAvailableCollection() {
    let availableCollection = [];
    DBSchemalList.forEach(function(schemaRule) {
      availableCollection.push(schemaRule.collection);
    });
    return availableCollection;
  }
  
  export function getSchemaByCollectionName(stringa) : any{
  
    let result = null;
    DBSchemalList.forEach(function(schemaRule) {
      if(schemaRule.collection == stringa) {
        result = schemaRule.schema;
        return;
      }
    });
    return result;
  }