
export function createResponse(res, message, body) {

  if(body.errorStatus) {
    createErrorResponse(res, body.errorKey, body.errorMessage, body.errorStatus);
  } else {
    res.json({
      code: res.statusCode,
      body: body,
      message: message
    });
  }
}

export function createErrorResponse(res, errorKey: string, errorMessage: string, statusCode?: number) {

  if(statusCode) {
    res.statusCode = statusCode;
  } else {
    res.statusCode = 404;
  }

  res.json({
    code:res.statusCode,
    errorKey: errorKey,
    errorMessage: errorMessage
  });
}
