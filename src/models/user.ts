import moment = require("moment");

export interface IUser {
  firstName: string|any;
  lastName: string|any;
  birthDate: Date|any;
  password: string|any;
  phoneNumber: string|any;
  email: string|any;
  sex: number|any;
  personalNumber:string|any;
  domicileCity:string|any;
  domicileAddress:string|any;
  residencyCity:string|any;
  residencyAddress:string|any;
  openID:string|any;
  emergencyPhoneNumber:string|any;
  lastUpdate?:Date|any;
  lastWeeklyUpdate?:Date|any;
}
export class User implements IUser {
  openID: string;
  firstName: string;
  lastName: string;
  birthDate: Date;
  password: string;
  phoneNumber: string;
  email: string;
  sex: number;
  personalNumber: string;
  domicileCity: string;
  domicileAddress: string;
  residencyCity: string;
  residencyAddress: string;
  emergencyPhoneNumber:string;
  lastUpdate:Date;
  lastWeeklyUpdate:Date;
  get age(){
    console.log("age");
    return moment().diff(moment(this.birthDate),"years");
  }
}
