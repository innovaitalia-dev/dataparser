export interface IModelTypes {
    name: string | any;
    collection: string | any;
    schema: any;
}

export class ModelTypes implements IModelTypes {

    name: string;
    collection: string;
    schema: any;
}