import { level } from "winston";

export interface ITemplate {
    name: string | any;
    fields: ITemplateFieldData[] | any;
}

export class Template implements ITemplate {
    name: string;
    fields: TemplateFieldData[];
}

export interface ITemplateFieldData {
    toKey: string | any;
    fromKey: string | any;
    fromCollection: string | any;
    container?: string | any;
    valueCatched?: string | any;
}

export class TemplateFieldData implements ITemplateFieldData {
    toKey: string;
    fromKey: string;
    fromCollection: string;
    container?: string;
    valueCatched?: string;
}

export interface IParsedDataModel {
    displayName: string | any;
    labelName: string | any;
    value?:  any;
    subFields?: IParsedDataModel[] | any[];
}

export class ParsedDataModel implements IParsedDataModel {
    displayName: string;
    labelName: string;
    value?:  any;
    subFields?: ParsedDataModel[];
}