Il progetto usa Typescript ed express.js.

Il progetto è diviso in tanti piccoli moduli per ragioni di scalabilità e collaborazione.

Viene usato **Inversify.js** per la dependency injection.

Per la connessione con il DB viene usato **Mongoose**.

Nei servizi esiste già un adapter che permette la comunicazione con l'assistant basato sulle API v2.


**Installazione**


*  Sulla base del file `.env.example` bisogna creare il file `.env.` Considerate che il file `.env` non viene pushato (best practice) le modifiche che apportate al `.env` riportatele anche in.env.example.
*  dal terminale eseguire `npm install`
*  dal terminale eseguire `npm run build:watch` (per compilare i file ts in JS)
*  dal terminale eseguire `npm run start:dev` (per seguire l'applicazione)

Il prerequisito è avere installato **npm e node.js**